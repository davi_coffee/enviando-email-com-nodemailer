const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const upload = require('multer')()

app.use(require('cors')())
app.use(bodyParser.json())

app.get('/', (req, res) => {
	return res.send('OI BRANCO')
})

app.post('/send', upload.single('anexo') , (req, res, next) => {
	const nome = req.body.nome
	const email = req.body.email
	const mensagem = req.body.mensagem
	const anexo = req.file
	require('./nodemail')(email, nome, mensagem, anexo).then(res => res.json(res)).catch(err => res.json(err))
})

app.listen(3000, () => {
	console.log('oi')
	
})



