const mailer = require("nodemailer");
require('dotenv/config')

module.exports = (email, nome, mensagem, anexo) => {
  const smtpTransport = mailer.createTransport({
    host: "smtp.gmail.com",
    port: "587",
    secure: false,
    auth: {
      user: process.env.MY_EMAIL,
      pass: process.env.MY_PASS,
    },
  });

  const mail = {
    from: "Davi <code.easy.31415@gmail.com",
    to: email,
    subject: `${nome} te enviou uma mensagem`,
    text: mensagem,
    html: "<h1>Ola</h1>",
  };

  if (anexo) {
    console.log(anexo);
    mail.attachments = [];
    mail.attachments.push({
      filename: anexo.originalname,
      content: anexo.buffer,
    });
  }

  return new Promise((resolve, reject) => {
    smtpTransport
      .sendMail(mail)
      .then((response) => {
        smtpTransport.close();
        return resolve(response);
      })
      .catch((error) => {
        smtpTransport.close();
		console.log('erro burrão')
        return reject(error);
      });
  });
};
